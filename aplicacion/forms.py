from django import forms
from models import Documento, Archivos

class DocumentoForm(forms.ModelForm):
	class Meta:
		model = Documento
		fields = ['titulo','fecha','abstracto','comentarios','numPaginas', 'palClave', 'url']