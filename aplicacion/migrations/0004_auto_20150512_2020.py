# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aplicacion', '0003_auto_20150512_1948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='palClave',
            field=models.CharField(max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='documento',
            name='url',
            field=models.CharField(max_length=200),
            preserve_default=True,
        ),
    ]
