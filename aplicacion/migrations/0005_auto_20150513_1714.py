# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aplicacion', '0004_auto_20150512_2020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='titulo',
            field=models.CharField(max_length=100),
            preserve_default=True,
        ),
    ]
