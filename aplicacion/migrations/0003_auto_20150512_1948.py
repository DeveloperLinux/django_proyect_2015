# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aplicacion', '0002_archivos'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='documento',
            name='autores',
        ),
        migrations.RemoveField(
            model_name='documento',
            name='editorial',
        ),
        migrations.RemoveField(
            model_name='documento',
            name='publiDate',
        ),
    ]
