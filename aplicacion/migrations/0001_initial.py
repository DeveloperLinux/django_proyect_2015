# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Documento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=30)),
                ('fecha', models.DateField()),
                ('editorial', models.CharField(max_length=30)),
                ('autores', models.CharField(max_length=50)),
                ('publiDate', models.DateField()),
                ('abstracto', models.TextField()),
                ('comentarios', models.TextField()),
                ('numPaginas', models.IntegerField()),
                ('palClave', models.CharField(max_length=100)),
                ('url', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
