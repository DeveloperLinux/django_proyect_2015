from django.db import models
from django.contrib import admin

# Create your models here.

class Documento(models.Model):
	titulo = models.CharField(max_length = 100)
	fecha = models.DateField()
	#editorial = models.CharField(max_length = 30)
	#autores = models.CharField(max_length = 50)
	#publiDate = models.DateField()
	abstracto = models.TextField()
	comentarios = models.TextField()
	numPaginas = models.IntegerField()
	palClave = models.CharField(max_length = 500)
	url = models.CharField(max_length = 200)
	#oculto = models.IntegerField()

	def __str__(self):
		return self.titulo

class Archivos(models.Model):
	files = models.FileField(upload_to = 'aplicacion/archivos')