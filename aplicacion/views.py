from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import loader, Context
from models import Documento
from models import Archivos
from django.db.models import Q
from forms import DocumentoForm
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
import xml.etree.ElementTree as ET


# Create your views here.

def home(request):
	articulos = list(Documento.objects.all())
	home = "home"
	contexto = Context({"home": home, "articulos": articulos})

	return render_to_response("principal.html", contexto)

def buscar(request):
	titulo = request.GET.get('titulo','')
	fecha_y = request.GET.get('fecha_y','')
	fecha_m = request.GET.get('fecha_m','')
	fecha_d = request.GET.get('fecha_d','')
	#editorial = request.GET.get('editorial','')
	#autores = request.GET.get('autores','')
	#pubdate = request.GET.get('pubdate','')
	abstracto = request.GET.get('abstracto','')
	comentarios = request.GET.get('comentarios','')
	numpag = request.GET.get('numpag','')
	palcla = request.GET.get('palcla','')
	url = request.GET.get('url','')
	query = ''
	
	if (titulo) or (fecha_y) or (fecha_m) or (fecha_d) or (abstracto) or (comentarios) or (numpag) or (palcla) or (url):
		if titulo:
			query = titulo
			qset = Q(titulo__icontains = titulo)
		elif (fecha_y) and (fecha_m) and (fecha_d):
			query = fecha_y
			qset = (
				Q(fecha__year = fecha_y) |
				Q(fecha__month = fecha_m) |
				Q(fecha__day = fecha_d)
				)
		elif fecha_y:
			query = fecha_y
			qset = Q(fecha__year = fecha_y)
		elif fecha_m:
			query = fecha_m
			qset = Q(fecha__month = fecha_m)
		elif fecha_d:
			query = fecha_d
			qset = Q(fecha__day = fecha_d)
		elif abstracto:
			query = abstracto
			qset = Q(abstracto__icontains = abstracto)
		elif comentarios:
			query = comentarios
			qset = Q(comentarios__icontains = comentarios)
		elif numpag:
			query = numpag
			qset = Q(numPaginas = numpag)
		elif palcla:
			query = palcla
			qset = Q(palClave__icontains = palcla)
		elif url:
			query = url
			qset = Q(url__icontains = url)
		resultados = Documento.objects.filter(qset)

	else:
		resultados = []
	
	return render_to_response("buscar.html", {
		'resultados': resultados,
		'query': query
		})
	

def insertar(request):
	files = list(Archivos.objects.all())
	
	cargar = request.POST.get('pk','')
	if 'cargar' in request.POST:
		archi = Archivos.objects.filter(pk=cargar)
		tree = ET.parse(archi[0].files)
		
		eliminar = Archivos.objects.filter(pk=cargar)
		eliminar.delete()
		
		tree.getroot()
		
		
		titulo = tree.findall('Entrezgene/Entrezgene_source/BioSource/BioSource_org/Org-ref/Org-ref_taxname')
		if titulo: titulo = titulo[0].text 
		else: titulo = "No disponible"
		
		fecha_y = tree.findall('Entrezgene/Entrezgene_track-info/Gene-track/Gene-track_create-date/Date/Date_std/Date-std/Date-std_year')
		if fecha_y: fecha_y = fecha_y[0].text 
		else: fecha_y = "No disponible"
		
		fecha_m = tree.findall('Entrezgene/Entrezgene_track-info/Gene-track/Gene-track_create-date/Date/Date_std/Date-std/Date-std_month')
		if fecha_m: fecha_m = fecha_m[0].text 
		else: fecha_m = "No disponible"
		
		fecha_d = tree.findall('Entrezgene/Entrezgene_track-info/Gene-track/Gene-track_create-date/Date/Date_std/Date-std/Date-std_day')
		if fecha_d: fecha_d = fecha_d[0].text 
		else: fecha_d = "No disponible"
		
		#editorial =
		#autores = 
		
		abstracto = tree.findall('Entrezgene/Entrezgene_summary')
		if abstracto: abstracto = abstracto[0].text
		else: abstracto = "No disponible"
			
		comentarios = tree.findall('Entrezgene/Entrezgene_comments/Gene-commentary[3]/Gene-commentary_comment/Gene-commentary[1]/Gene-commentary_heading')
		if comentarios: comentarios = comentarios[0].text
		else: comentarios = "No disponible"
		
		numpag = tree.findall('Entrezgene/Entrezgene_comments/Gene-commentary[1]/Gene-commentary_type')
		if numpag: numpag = numpag[0].text
		else: numpag = "No disponible"
		
		palcla = tree.findall('Entrezgene/Entrezgene_source/BioSource/BioSource_org/Org-ref/Org-ref_orgname/OrgName/OrgName_lineage')
		if palcla: palcla = palcla[0].text
		else: palcla = "No disponible"
		
		url = tree.findall('Entrezgene/Entrezgene_comments/Gene-commentary/Gene-commentary_comment/Gene-commentary/Gene-commentary_comment/Gene-commentary/Gene-commentary_source/Other-source/Other-source_url')
		if url: 
			url = url[0].text 
		else:
			opc2 = tree.findall('/Entrezgene/Entrezgene_comments/Gene-commentary/Gene-commentary_comment/Gene-commentary/Gene-commentary_source/Other-source/Other-source_url')
			if opc2:
				url = opc2[0].text
			else:
				opc3 = tree.findall('Entrezgene/Entrezgene_comments/Gene-commentary/Gene-commentary_comment/Gene-commentary/Gene-commentary_source/Other-source/Other-source_url')
				if opc3:
					url = opc3[0].text
				else:
					opc4 = tree.findall('Entrezgene/Entrezgene_comments/Gene-commentary/Gene-commentary_comment/Gene-commentary/Gene-commentary_source/Other-source/Other-source_url')
					if opc4:
						url = opc4[0].text
						
					else:
						opc5 = tree.findall('Entrezgene/Entrezgene_comments/Gene-commentary[5]/Gene-commentary_comment/Gene-commentary[2]/Gene-commentary_source/Other-source/Other-source_url')
						if opc5:
							url = opc5[0].text
						else:
							url = "No disponible"
				
		
	else:
		titulo = ""
		fecha_y = ""
		fecha_m = ""
		fecha_d = ""
		#editorial =
		#autores = 
		abstracto = ""
		comentarios = ""
		numpag = ""
		palcla = ""
		url = ""
	
	if 'submit' in request.POST:
		form = DocumentoForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/insertar/')
	else:
		form = DocumentoForm()

	args = {}
	args.update(csrf(request))

	args['form'] = form
	archivos = Archivos()
	
	contexto = Context({'inserta':'insertar','files':files, 'fecha_y':fecha_y, 'fecha_d':fecha_d, 'fecha_m':fecha_m, 'cargar':cargar,
	'titulo':titulo, 'url':url, 'abstracto':abstracto, 'palcla':palcla, 'numpag':numpag, 'comentarios':comentarios})

	return render_to_response('principal.html', args, contexto)

def eliminar(request):
	titulo = request.GET.get('titulo', '')
	if titulo:
		qset = (
			Q(titulo__icontains = titulo)
			)
		resultados = Documento.objects.filter(qset)
		resultados.delete()
	
	articulos = list(Documento.objects.all())
	contexto = Context({"articulos": articulos, "titulo": titulo})

	return render_to_response("eliminar.html", contexto)