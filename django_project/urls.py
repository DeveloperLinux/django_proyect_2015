from django.conf.urls import patterns, include, url
#from django.views.defaults import *
#from aplicacion.views import vista404

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'aplicacion.views.home', name='home'),

    url(r'^eliminar/', 'aplicacion.views.eliminar', name='eliminar'),
    url(r'^buscar/', 'aplicacion.views.buscar', name='buscar'),
    url(r'^insertar/', 'aplicacion.views.insertar', name='insertar'),
    url(r'^admin/', include(admin.site.urls)),
)